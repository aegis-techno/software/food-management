package fr.aegistechno.foodmanagement.ui.shopping;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import fr.aegistechno.foodmanagement.databinding.FragmentShoppingBinding;

public class ShoppingFragment extends Fragment {

    private ShoppingViewModel shoppingViewModel;
    private FragmentShoppingBinding binding;

    public View onCreateView(
        @NonNull
        LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        shoppingViewModel = new ViewModelProvider(this).get(ShoppingViewModel.class);

        binding = FragmentShoppingBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textShopping;
        shoppingViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(
                @Nullable
                String s
            ) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}