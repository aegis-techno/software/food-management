package fr.aegistechno.foodmanagement.ui.products;

import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;
import fr.aegistechno.foodmanagement.R;
import fr.aegistechno.foodmanagement.domain.Product;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private int productItemLayout;
    private List<Product> productList;
    private ProductsViewModel productsViewModel;

    public ProductListAdapter(int layoutId, final ProductsViewModel productsViewModel) {
        this.productItemLayout = layoutId;
        this.productsViewModel = productsViewModel;
    }

    public void setProductList(List<Product> products) {
        productList = products;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(productItemLayout, parent, false);
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        final Product product = productList.get(listPosition);

        holder.name.setText(product.getName());
        holder.category.setText(product.getCategory());
        holder.btn_delete.setOnClickListener(view -> {

            AlertDialog.Builder alert = new AlertDialog.Builder(view.getContext());
            alert.setTitle("Delete product");
            alert.setMessage("Are you sure you want to delete?");
            alert.setPositiveButton(android.R.string.yes,
                                    (dialog, which) -> productsViewModel.deleteProduct(product.getId()));
            alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
            alert.show();
        });
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView category;
        AppCompatImageButton btn_delete;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.product_row_name);
            category = itemView.findViewById(R.id.product_row_category);
            btn_delete = itemView.findViewById(R.id.btn_delete_product);
        }

    }

}
