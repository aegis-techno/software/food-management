package fr.aegistechno.foodmanagement.ui.products;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import fr.aegistechno.foodmanagement.R;
import fr.aegistechno.foodmanagement.databinding.FragmentProductsBinding;
import fr.aegistechno.foodmanagement.domain.Product;

public class ProductsFragment extends Fragment {

    private ProductsViewModel productsViewModel;
    private ProductListAdapter adapter;
    private FragmentProductsBinding binding;

    public View onCreateView(
        @NonNull
        LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        productsViewModel = new ViewModelProvider(this).get(ProductsViewModel.class);

        binding = FragmentProductsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        adapter = new ProductListAdapter(R.layout.product_list_item, productsViewModel);
        RecyclerView recyclerView = root.findViewById(R.id.product_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        productsViewModel.getAllProducts()
            .observe(getViewLifecycleOwner(), products -> adapter.setProductList(products));

        FloatingActionButton btnAddProduct = root.findViewById(R.id.btn_add_product);
        btnAddProduct.setOnClickListener(v -> {
            final Product product = new Product();
            product.setName("New product");
            product.setCategory("Category");
            productsViewModel.insertProduct(product);
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}