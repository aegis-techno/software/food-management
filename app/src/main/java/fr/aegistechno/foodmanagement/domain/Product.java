package fr.aegistechno.foodmanagement.domain;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity(tableName = "products")
public class Product {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "category")
    private String category;

    @ColumnInfo(name = "quantity")
    private int quantity;

    @ColumnInfo(name = "unit")
    private String unit;

    @ColumnInfo(name = "price")
    private int price;

    @ColumnInfo(name = "expirationDate")
    private String expirationDate;

}
