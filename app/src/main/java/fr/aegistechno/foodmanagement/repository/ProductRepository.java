package fr.aegistechno.foodmanagement.repository;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import fr.aegistechno.foodmanagement.domain.Product;

import java.util.List;

public class ProductRepository {

    private MutableLiveData<List<Product>> searchResults = new MutableLiveData<>();
    private LiveData<List<Product>> allProducts;
    private ProductDao productDao;

    public ProductRepository(Application application) {
        ProductRoomDatabase db;
        db = ProductRoomDatabase.getDatabase(application);
        productDao = db.productDao();
        allProducts = productDao.getAllProducts();
    }

    private void asyncFinished(List<Product> results) {
        searchResults.setValue(results);
    }

    public void insertProduct(Product newProduct) {
        InsertAsyncTask task = new InsertAsyncTask(productDao);
        task.execute(newProduct);
    }

    public void deleteProduct(int id) {
        DeleteAsyncTask task = new DeleteAsyncTask(productDao);
        task.execute(id);
    }

    public void findProduct(String name) {
        QueryAsyncTask task = new QueryAsyncTask(productDao);
        task.delegate = this;
        task.execute(name);
    }

    public LiveData<List<Product>> getAllProducts() {
        return allProducts;
    }

    public MutableLiveData<List<Product>> getSearchResults() {
        return searchResults;
    }

    private static class QueryAsyncTask extends AsyncTask<String, Void, List<Product>> {

        private ProductDao asyncTaskDao;
        private ProductRepository delegate = null;

        QueryAsyncTask(ProductDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<Product> doInBackground(final String... params) {
            return asyncTaskDao.findByName(params[0]);
        }

        @Override
        protected void onPostExecute(List<Product> result) {
            delegate.asyncFinished(result);
        }

    }

    private static class InsertAsyncTask extends AsyncTask<Product, Void, Void> {

        private ProductDao asyncTaskDao;

        InsertAsyncTask(ProductDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Product... params) {
            asyncTaskDao.insertProduct(params[0]);
            return null;
        }

    }

    private static class DeleteAsyncTask extends AsyncTask<Integer, Void, Void> {

        private ProductDao asyncTaskDao;

        DeleteAsyncTask(ProductDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            asyncTaskDao.deleteById(params[0]);
            return null;
        }

    }

}