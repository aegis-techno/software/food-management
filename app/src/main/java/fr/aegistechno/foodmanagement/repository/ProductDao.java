package fr.aegistechno.foodmanagement.repository;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import fr.aegistechno.foodmanagement.domain.Product;

import java.util.List;

@Dao
public interface ProductDao {

    @Insert
    void insertProduct(Product product);

    @Query("SELECT * FROM products WHERE name = :name")
    List<Product> findByName(String name);

    @Query("DELETE FROM products WHERE id = :id")
    void deleteById(int id);

    @Query("SELECT * FROM products")
    LiveData<List<Product>> getAllProducts();

}